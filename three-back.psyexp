﻿<?xml version="1.0" ?>
<PsychoPy2experiment encoding="utf-8" version="2020.1.0">
  <Settings>
    <Param name="Audio latency priority" updates="None" val="use prefs" valType="str"/>
    <Param name="Audio lib" updates="None" val="use prefs" valType="str"/>
    <Param name="Completed URL" updates="None" val="" valType="str"/>
    <Param name="Data filename" updates="None" val="u'data/%s/%s_%s_%s' % (expInfo['participant'], expInfo['participant'], expName, expInfo['date'])" valType="code"/>
    <Param name="Enable Escape" updates="None" val="True" valType="bool"/>
    <Param name="Experiment info" updates="None" val="{'participant': '', 'session': '001', 'backN': '3', 'probability': '0.2'}" valType="code"/>
    <Param name="Force stereo" updates="None" val="True" valType="bool"/>
    <Param name="Full-screen window" updates="None" val="True" valType="bool"/>
    <Param name="HTML path" updates="None" val="html" valType="str"/>
    <Param name="Incomplete URL" updates="None" val="" valType="str"/>
    <Param name="JS libs" updates="None" val="packaged" valType="str"/>
    <Param name="Monitor" updates="None" val="testMonitor" valType="str"/>
    <Param name="Save csv file" updates="None" val="False" valType="bool"/>
    <Param name="Save excel file" updates="None" val="False" valType="bool"/>
    <Param name="Save log file" updates="None" val="True" valType="bool"/>
    <Param name="Save psydat file" updates="None" val="True" valType="bool"/>
    <Param name="Save wide csv file" updates="None" val="True" valType="bool"/>
    <Param name="Screen" updates="None" val="1" valType="num"/>
    <Param name="Show info dlg" updates="None" val="True" valType="bool"/>
    <Param name="Show mouse" updates="None" val="False" valType="bool"/>
    <Param name="Units" updates="None" val="deg" valType="str"/>
    <Param name="Use version" updates="None" val="" valType="str"/>
    <Param name="Window size (pixels)" updates="None" val="[1920, 1080]" valType="code"/>
    <Param name="blendMode" updates="None" val="avg" valType="str"/>
    <Param name="color" updates="None" val="$[1,1,1]" valType="str"/>
    <Param name="colorSpace" updates="None" val="rgb" valType="str"/>
    <Param name="expName" updates="None" val="three-back" valType="str"/>
    <Param name="exportHTML" updates="None" val="on Sync" valType="str"/>
    <Param name="logging level" updates="None" val="exp" valType="code"/>
  </Settings>
  <Routines>
    <Routine name="Instructions">
      <TextComponent name="instructions">
        <Param name="color" updates="constant" val="black" valType="str"/>
        <Param name="colorSpace" updates="constant" val="rgb" valType="str"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="flip" updates="constant" val="" valType="str"/>
        <Param name="font" updates="constant" val="Arial" valType="str"/>
        <Param name="languageStyle" updates="None" val="LTR" valType="str"/>
        <Param name="letterHeight" updates="constant" val="1" valType="code"/>
        <Param name="name" updates="None" val="instructions" valType="code"/>
        <Param name="opacity" updates="constant" val="1" valType="code"/>
        <Param name="ori" updates="constant" val="0" valType="code"/>
        <Param name="pos" updates="constant" val="(0, 0)" valType="code"/>
        <Param name="saveStartStop" updates="None" val="False" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="0.0" valType="code"/>
        <Param name="stopType" updates="None" val="condition" valType="str"/>
        <Param name="stopVal" updates="constant" val="False" valType="code"/>
        <Param name="syncScreenRefresh" updates="None" val="True" valType="bool"/>
        <Param name="text" updates="constant" val="$&quot;The n-back task presents stimuli sequentially and require you to decide if the current stimulus is the same as one presented n-numbers previous (or back).\n&quot;&amp;#10;+ &quot;\n&quot;&amp;#10;+ &quot;For example, the 3-back you will indicate if the current number matches the number presented 3 places previous\n&quot;&amp;#10;+ &quot;(e.g.  A H G A B G B shows that 'A' and 'G' have matches in this sequence).\n&quot;&amp;#10;+ &quot;\n&quot;&amp;#10;+ f&quot;This is an {expInfo['backN']}-back task.\n&quot;&amp;#10;+ f&quot;Press SPACE if the current stimulus is the same a {expInfo['backN']} places previous.\n&quot;&amp;#10;+ &quot;\n&quot;&amp;#10;+ &quot;The task will start after some minutes of rest.&quot;" valType="str"/>
        <Param name="units" updates="None" val="from exp settings" valType="str"/>
        <Param name="wrapWidth" updates="constant" val="45" valType="code"/>
      </TextComponent>
      <KeyboardComponent name="instructions_key">
        <Param name="allowedKeys" updates="constant" val="'space'" valType="code"/>
        <Param name="correctAns" updates="constant" val="" valType="str"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="discard previous" updates="constant" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="forceEndRoutine" updates="constant" val="True" valType="bool"/>
        <Param name="name" updates="None" val="instructions_key" valType="code"/>
        <Param name="saveStartStop" updates="None" val="False" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="0.0" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="" valType="code"/>
        <Param name="store" updates="constant" val="nothing" valType="str"/>
        <Param name="storeCorrect" updates="constant" val="False" valType="bool"/>
        <Param name="syncScreenRefresh" updates="constant" val="False" valType="bool"/>
      </KeyboardComponent>
      <VariableComponent name="stimuli">
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="name" updates="None" val="stimuli" valType="code"/>
        <Param name="saveEndExp" updates="constant" val="False" valType="bool"/>
        <Param name="saveEndRoutine" updates="constant" val="True" valType="bool"/>
        <Param name="saveFrameValue" updates="constant" val="never" valType="str"/>
        <Param name="saveStartExp" updates="constant" val="False" valType="bool"/>
        <Param name="saveStartRoutine" updates="constant" val="False" valType="bool"/>
        <Param name="saveStartStop" updates="None" val="False" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startExpValue" updates="constant" val="np.array(['A','B','C','D','E','F','G','H'])" valType="code"/>
        <Param name="startFrameValue" updates="None" val="" valType="code"/>
        <Param name="startRoutineValue" updates="constant" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="" valType="code"/>
        <Param name="syncScreenRefresh" updates="None" val="False" valType="bool"/>
      </VariableComponent>
    </Routine>
    <Routine name="trial_back">
      <TextComponent name="text">
        <Param name="color" updates="constant" val="white" valType="str"/>
        <Param name="colorSpace" updates="constant" val="rgb" valType="str"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="flip" updates="constant" val="" valType="str"/>
        <Param name="font" updates="constant" val="Arial" valType="str"/>
        <Param name="languageStyle" updates="None" val="LTR" valType="str"/>
        <Param name="letterHeight" updates="constant" val="2.5" valType="code"/>
        <Param name="name" updates="None" val="text" valType="code"/>
        <Param name="opacity" updates="constant" val="1" valType="code"/>
        <Param name="ori" updates="constant" val="0" valType="code"/>
        <Param name="pos" updates="constant" val="(0, 0)" valType="code"/>
        <Param name="saveStartStop" updates="None" val="True" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="0.0" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="1.0" valType="code"/>
        <Param name="syncScreenRefresh" updates="None" val="True" valType="bool"/>
        <Param name="text" updates="set every repeat" val="$[t['stimulus'] for t in loop_n.trialList]" valType="str"/>
        <Param name="units" updates="None" val="from exp settings" valType="str"/>
        <Param name="wrapWidth" updates="constant" val="" valType="code"/>
      </TextComponent>
    </Routine>
    <Routine name="trial">
      <CodeComponent name="trial_code">
        <Param name="Begin Experiment" updates="constant" val="prevN = np.array([])" valType="extendedCode"/>
        <Param name="Begin JS Experiment" updates="constant" val="" valType="extendedCode"/>
        <Param name="Begin JS Routine" updates="constant" val="if (((currentLoop.thisN &lt; Number.parseInt(expInfo[&quot;backN&quot;])) || (np.random.uniform() &gt; Number.parseFloat(expInfo[&quot;probability&quot;])))) {&amp;#10;    stimleft = np.delete(stimuli, function () {&amp;#10;    var _pj_a = [], _pj_b = prevN;&amp;#10;    for (var _pj_c = 0, _pj_d = _pj_b.length; (_pj_c &lt; _pj_d); _pj_c += 1) {&amp;#10;        var i = _pj_b[_pj_c];&amp;#10;        _pj_a.push(np.argwhere((stimuli === i)));&amp;#10;    }&amp;#10;    return _pj_a;&amp;#10;}&amp;#10;.call(this));&amp;#10;    stimulus = np.random.choice(stimleft);&amp;#10;    correctResponse = null;&amp;#10;} else {&amp;#10;    stimulus = prevN.slice((- Number.parseInt(expInfo[&quot;backN&quot;])))[0];&amp;#10;    correctResponse = &quot;space&quot;;&amp;#10;}&amp;#10;" valType="extendedCode"/>
        <Param name="Begin Routine" updates="constant" val="if (currentLoop.thisN &lt; int(expInfo['backN'])) or (np.random.uniform() &gt; float(expInfo['probability'])):&amp;#10;    stimleft = np.delete(stimuli,[np.argwhere(stimuli==i) for i in prevN])&amp;#10;    stimulus = np.random.choice(stimleft)&amp;#10;    thisExp.addData('trial_type', 'none')&amp;#10;    correctResponse = None&amp;#10;else:&amp;#10;    stimulus = prevN[-int(expInfo['backN'])]&amp;#10;    thisExp.addData('trial_type', 'back')&amp;#10;    correctResponse = 'space'" valType="extendedCode"/>
        <Param name="Code Type" updates="None" val="Auto-&gt;JS" valType="str"/>
        <Param name="Each Frame" updates="constant" val="" valType="extendedCode"/>
        <Param name="Each JS Frame" updates="constant" val="" valType="extendedCode"/>
        <Param name="End Experiment" updates="constant" val="trialNone = 0; trialBack = 0; hit = 0; miss = 0; fa = 0&amp;#10;for n, t in enumerate(thisExp.entries):&amp;#10;    if t['trial_type'] == 'none':&amp;#10;        trialNone += 1&amp;#10;        if not(t['trial_resp.corr']): fa += 1&amp;#10;    if t['trial_type'] == 'back':&amp;#10;        trialBack += 1&amp;#10;        if t['trial_resp.corr']: hit += 1&amp;#10;        else: miss += 1&amp;#10;logging.log(level=logging.DATA, msg='hit={:.2f}%,miss={:.2f}%,fa={:.2f}%'.format(hit/trialBack*100,miss/trialBack*100,fa/trialNone*100))&amp;#10;" valType="extendedCode"/>
        <Param name="End JS Experiment" updates="constant" val="" valType="extendedCode"/>
        <Param name="End JS Routine" updates="constant" val="" valType="extendedCode"/>
        <Param name="End Routine" updates="constant" val="thisExp.addData('tial_stimulus.stim', stimulus)&amp;#10;prevN = np.hstack((prevN,stimulus))&amp;#10;prevN = prevN[-3:]" valType="extendedCode"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="name" updates="None" val="trial_code" valType="code"/>
      </CodeComponent>
      <TextComponent name="trial_stimulus">
        <Param name="color" updates="constant" val="black" valType="str"/>
        <Param name="colorSpace" updates="constant" val="rgb" valType="str"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="flip" updates="constant" val="" valType="str"/>
        <Param name="font" updates="constant" val="Arial" valType="str"/>
        <Param name="languageStyle" updates="None" val="LTR" valType="str"/>
        <Param name="letterHeight" updates="constant" val="2.5" valType="code"/>
        <Param name="name" updates="None" val="trial_stimulus" valType="code"/>
        <Param name="opacity" updates="constant" val="1" valType="code"/>
        <Param name="ori" updates="constant" val="0" valType="code"/>
        <Param name="pos" updates="constant" val="(0, 0)" valType="code"/>
        <Param name="saveStartStop" updates="None" val="True" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="0" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="0.03" valType="code"/>
        <Param name="syncScreenRefresh" updates="None" val="True" valType="bool"/>
        <Param name="text" updates="set every repeat" val="$stimulus" valType="str"/>
        <Param name="units" updates="None" val="from exp settings" valType="str"/>
        <Param name="wrapWidth" updates="constant" val="" valType="code"/>
      </TextComponent>
      <KeyboardComponent name="trial_resp">
        <Param name="allowedKeys" updates="constant" val="'space'" valType="code"/>
        <Param name="correctAns" updates="constant" val="$correctResponse" valType="str"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="discard previous" updates="constant" val="True" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="forceEndRoutine" updates="constant" val="False" valType="bool"/>
        <Param name="name" updates="None" val="trial_resp" valType="code"/>
        <Param name="saveStartStop" updates="None" val="True" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="0.0" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="2" valType="code"/>
        <Param name="store" updates="constant" val="first key" valType="str"/>
        <Param name="storeCorrect" updates="constant" val="True" valType="bool"/>
        <Param name="syncScreenRefresh" updates="constant" val="True" valType="bool"/>
      </KeyboardComponent>
    </Routine>
    <Routine name="Rest">
      <PolygonComponent name="rest_bg">
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="fillColor" updates="constant" val="$[1,1,1]" valType="str"/>
        <Param name="fillColorSpace" updates="constant" val="rgb" valType="str"/>
        <Param name="interpolate" updates="constant" val="linear" valType="str"/>
        <Param name="lineColor" updates="constant" val="$[1,1,1]" valType="str"/>
        <Param name="lineColorSpace" updates="constant" val="rgb" valType="str"/>
        <Param name="lineWidth" updates="constant" val="0" valType="code"/>
        <Param name="nVertices" updates="constant" val="4" valType="int"/>
        <Param name="name" updates="None" val="rest_bg" valType="code"/>
        <Param name="opacity" updates="constant" val="1" valType="code"/>
        <Param name="ori" updates="constant" val="0" valType="code"/>
        <Param name="pos" updates="constant" val="(0, 0)" valType="code"/>
        <Param name="saveStartStop" updates="None" val="True" valType="bool"/>
        <Param name="shape" updates="constant" val="rectangle" valType="str"/>
        <Param name="size" updates="constant" val="(2, 2)" valType="code"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="0.0" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="300" valType="code"/>
        <Param name="syncScreenRefresh" updates="None" val="True" valType="bool"/>
        <Param name="units" updates="None" val="norm" valType="str"/>
      </PolygonComponent>
      <TextComponent name="rest_text">
        <Param name="color" updates="constant" val="black" valType="str"/>
        <Param name="colorSpace" updates="constant" val="rgb" valType="str"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="flip" updates="constant" val="" valType="str"/>
        <Param name="font" updates="constant" val="Arial" valType="str"/>
        <Param name="languageStyle" updates="None" val="LTR" valType="str"/>
        <Param name="letterHeight" updates="constant" val="1" valType="code"/>
        <Param name="name" updates="None" val="rest_text" valType="code"/>
        <Param name="opacity" updates="constant" val="1" valType="code"/>
        <Param name="ori" updates="constant" val="0" valType="code"/>
        <Param name="pos" updates="constant" val="(0, 0)" valType="code"/>
        <Param name="saveStartStop" updates="None" val="True" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="0.0" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="270" valType="code"/>
        <Param name="syncScreenRefresh" updates="None" val="True" valType="bool"/>
        <Param name="text" updates="constant" val="Rest" valType="str"/>
        <Param name="units" updates="None" val="from exp settings" valType="str"/>
        <Param name="wrapWidth" updates="constant" val="" valType="code"/>
      </TextComponent>
      <TextComponent name="rest_text2">
        <Param name="color" updates="constant" val="black" valType="str"/>
        <Param name="colorSpace" updates="constant" val="rgb" valType="str"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="durationEstim" updates="None" val="" valType="code"/>
        <Param name="flip" updates="constant" val="" valType="str"/>
        <Param name="font" updates="constant" val="Arial" valType="str"/>
        <Param name="languageStyle" updates="None" val="LTR" valType="str"/>
        <Param name="letterHeight" updates="constant" val="1" valType="code"/>
        <Param name="name" updates="None" val="rest_text2" valType="code"/>
        <Param name="opacity" updates="constant" val="1" valType="code"/>
        <Param name="ori" updates="constant" val="0" valType="code"/>
        <Param name="pos" updates="constant" val="(0, 0)" valType="code"/>
        <Param name="saveStartStop" updates="None" val="True" valType="bool"/>
        <Param name="startEstim" updates="None" val="" valType="code"/>
        <Param name="startType" updates="None" val="time (s)" valType="str"/>
        <Param name="startVal" updates="None" val="270" valType="code"/>
        <Param name="stopType" updates="None" val="duration (s)" valType="str"/>
        <Param name="stopVal" updates="constant" val="29" valType="code"/>
        <Param name="syncScreenRefresh" updates="None" val="True" valType="bool"/>
        <Param name="text" updates="constant" val="Get ready!" valType="str"/>
        <Param name="units" updates="None" val="from exp settings" valType="str"/>
        <Param name="wrapWidth" updates="constant" val="" valType="code"/>
      </TextComponent>
      <CodeComponent name="rest_code">
        <Param name="Begin Experiment" updates="constant" val="" valType="extendedCode"/>
        <Param name="Begin JS Experiment" updates="constant" val="" valType="extendedCode"/>
        <Param name="Begin JS Routine" updates="constant" val="" valType="extendedCode"/>
        <Param name="Begin Routine" updates="constant" val="" valType="extendedCode"/>
        <Param name="Code Type" updates="None" val="Auto-&gt;JS" valType="str"/>
        <Param name="Each Frame" updates="constant" val="if routineTimer.getTime() &lt;= 10:&amp;#10;    rest_text2.setText(&quot;Get ready!\n{}&quot;.format(round(routineTimer.getTime())), log=False)&amp;#10;" valType="extendedCode"/>
        <Param name="Each JS Frame" updates="constant" val="/* Syntax Error: Fix Python code */" valType="extendedCode"/>
        <Param name="End Experiment" updates="constant" val="" valType="extendedCode"/>
        <Param name="End JS Experiment" updates="constant" val="" valType="extendedCode"/>
        <Param name="End JS Routine" updates="constant" val="" valType="extendedCode"/>
        <Param name="End Routine" updates="constant" val="" valType="extendedCode"/>
        <Param name="disabled" updates="None" val="False" valType="bool"/>
        <Param name="name" updates="None" val="rest_code" valType="code"/>
      </CodeComponent>
    </Routine>
  </Routines>
  <Flow>
    <Routine name="Instructions"/>
    <Routine name="Rest"/>
    <LoopInitiator loopType="TrialHandler" name="loop">
      <Param name="Selected rows" updates="None" val="" valType="str"/>
      <Param name="conditions" updates="None" val="None" valType="str"/>
      <Param name="conditionsFile" updates="None" val="" valType="str"/>
      <Param name="endPoints" updates="None" val="[0, 1]" valType="num"/>
      <Param name="isTrials" updates="None" val="True" valType="bool"/>
      <Param name="loopType" updates="None" val="sequential" valType="str"/>
      <Param name="nReps" updates="None" val="150" valType="code"/>
      <Param name="name" updates="None" val="loop" valType="code"/>
      <Param name="random seed" updates="None" val="" valType="code"/>
    </LoopInitiator>
    <Routine name="trial"/>
    <LoopTerminator name="loop"/>
  </Flow>
</PsychoPy2experiment>
