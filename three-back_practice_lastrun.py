﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2020.1.0),
    on February 13, 2020, at 14:40
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2020.1.0'
expName = 'three-back_practice'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001', 'backN': '3', 'probability': '0.2'}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s/%s_%s_%s' % (expInfo['participant'], expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='C:\\Users\\nemo\\Desktop\\NEMO\\Paradigms\\three-back\\three-back_practice_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=[1920, 1080], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color=[1,1,1], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='deg')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "Instructions"
InstructionsClock = core.Clock()
instructions = visual.TextStim(win=win, name='instructions',
    text="The n-back task presents stimuli sequentially and require you to decide if the current stimulus is the same as one presented n-numbers previous (or back).\n"
+ "\n"
+ "For example, the 3-back you will indicate if the current number matches the number presented 3 places previous\n"
+ "(e.g.  A H G A B G B shows that 'A' and 'G' have matches in this sequence).\n"
+ "\n"
+ f"This is an {expInfo['backN']}-back task.\n"
+ f"Press SPACE if the current stimulus is the same a {expInfo['backN']} places previous.\n"
+ "\n"
+ "Press SPACE bar when ready to start." ,
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=30, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
instructions_key = keyboard.Keyboard()
# Set experiment start values for variable component stimuli
stimuli = np.array(['A','B','C','D','E','F','G','H'])
stimuliContainer = []

# Initialize components for Routine "trial"
trialClock = core.Clock()
prevN = np.array([])
trial_stimulus = visual.TextStim(win=win, name='trial_stimulus',
    text='default text',
    font='Arial',
    pos=(0, 0), height=2.5, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
trial_resp = keyboard.Keyboard()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "Instructions"-------
continueRoutine = True
# update component parameters for each repeat
instructions_key.keys = []
instructions_key.rt = []
_instructions_key_allKeys = []
# keep track of which components have finished
InstructionsComponents = [instructions, instructions_key]
for thisComponent in InstructionsComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
InstructionsClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions"-------
while continueRoutine:
    # get current time
    t = InstructionsClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=InstructionsClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instructions* updates
    if instructions.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instructions.frameNStart = frameN  # exact frame index
        instructions.tStart = t  # local t and not account for scr refresh
        instructions.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instructions, 'tStartRefresh')  # time at next scr refresh
        instructions.setAutoDraw(True)
    if instructions.status == STARTED:
        if bool(False):
            # keep track of stop time/frame for later
            instructions.tStop = t  # not accounting for scr refresh
            instructions.frameNStop = frameN  # exact frame index
            win.timeOnFlip(instructions, 'tStopRefresh')  # time at next scr refresh
            instructions.setAutoDraw(False)
    
    # *instructions_key* updates
    if instructions_key.status == NOT_STARTED and t >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        instructions_key.frameNStart = frameN  # exact frame index
        instructions_key.tStart = t  # local t and not account for scr refresh
        instructions_key.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(instructions_key, 'tStartRefresh')  # time at next scr refresh
        instructions_key.status = STARTED
        # keyboard checking is just starting
        instructions_key.clock.reset()  # now t=0
    if instructions_key.status == STARTED:
        theseKeys = instructions_key.getKeys(keyList=['space'], waitRelease=False)
        _instructions_key_allKeys.extend(theseKeys)
        if len(_instructions_key_allKeys):
            instructions_key.keys = _instructions_key_allKeys[-1].name  # just the last key pressed
            instructions_key.rt = _instructions_key_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in InstructionsComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions"-------
for thisComponent in InstructionsComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "Instructions" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
loop = data.TrialHandler(nReps=50, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=[None],
    seed=None, name='loop')
thisExp.addLoop(loop)  # add the loop to the experiment
thisLoop = loop.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisLoop.rgb)
if thisLoop != None:
    for paramName in thisLoop:
        exec('{} = thisLoop[paramName]'.format(paramName))

for thisLoop in loop:
    currentLoop = loop
    # abbreviate parameter names if possible (e.g. rgb = thisLoop.rgb)
    if thisLoop != None:
        for paramName in thisLoop:
            exec('{} = thisLoop[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "trial"-------
    continueRoutine = True
    routineTimer.add(2.000000)
    # update component parameters for each repeat
    if (currentLoop.thisN < int(expInfo['backN'])) or (np.random.uniform() > float(expInfo['probability'])):
        stimleft = np.delete(stimuli,[np.argwhere(stimuli==i) for i in prevN])
        stimulus = np.random.choice(stimleft)
        thisExp.addData('trial_type', 'none')
        correctResponse = None
    else:
        stimulus = prevN[-int(expInfo['backN'])]
        thisExp.addData('trial_type', 'back')
        correctResponse = 'space'
    trial_stimulus.setText(stimulus)
    trial_resp.keys = []
    trial_resp.rt = []
    _trial_resp_allKeys = []
    # keep track of which components have finished
    trialComponents = [trial_stimulus, trial_resp]
    for thisComponent in trialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "trial"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = trialClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=trialClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *trial_stimulus* updates
        if trial_stimulus.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
            # keep track of start time/frame for later
            trial_stimulus.frameNStart = frameN  # exact frame index
            trial_stimulus.tStart = t  # local t and not account for scr refresh
            trial_stimulus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(trial_stimulus, 'tStartRefresh')  # time at next scr refresh
            trial_stimulus.setAutoDraw(True)
        if trial_stimulus.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > trial_stimulus.tStartRefresh + 0.03-frameTolerance:
                # keep track of stop time/frame for later
                trial_stimulus.tStop = t  # not accounting for scr refresh
                trial_stimulus.frameNStop = frameN  # exact frame index
                win.timeOnFlip(trial_stimulus, 'tStopRefresh')  # time at next scr refresh
                trial_stimulus.setAutoDraw(False)
        
        # *trial_resp* updates
        waitOnFlip = False
        if trial_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            trial_resp.frameNStart = frameN  # exact frame index
            trial_resp.tStart = t  # local t and not account for scr refresh
            trial_resp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(trial_resp, 'tStartRefresh')  # time at next scr refresh
            trial_resp.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(trial_resp.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(trial_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if trial_resp.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > trial_resp.tStartRefresh + 2-frameTolerance:
                # keep track of stop time/frame for later
                trial_resp.tStop = t  # not accounting for scr refresh
                trial_resp.frameNStop = frameN  # exact frame index
                win.timeOnFlip(trial_resp, 'tStopRefresh')  # time at next scr refresh
                trial_resp.status = FINISHED
        if trial_resp.status == STARTED and not waitOnFlip:
            theseKeys = trial_resp.getKeys(keyList=['space'], waitRelease=False)
            _trial_resp_allKeys.extend(theseKeys)
            if len(_trial_resp_allKeys):
                trial_resp.keys = _trial_resp_allKeys[0].name  # just the first key pressed
                trial_resp.rt = _trial_resp_allKeys[0].rt
                # was this correct?
                if (trial_resp.keys == str(correctResponse)) or (trial_resp.keys == correctResponse):
                    trial_resp.corr = 1
                else:
                    trial_resp.corr = 0
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "trial"-------
    for thisComponent in trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('tial_stimulus.stim', stimulus)
    prevN = np.hstack((prevN,stimulus))
    prevN = prevN[-3:]
    loop.addData('trial_stimulus.started', trial_stimulus.tStartRefresh)
    loop.addData('trial_stimulus.stopped', trial_stimulus.tStopRefresh)
    # check responses
    if trial_resp.keys in ['', [], None]:  # No response was made
        trial_resp.keys = None
        # was no response the correct answer?!
        if str(correctResponse).lower() == 'none':
           trial_resp.corr = 1;  # correct non-response
        else:
           trial_resp.corr = 0;  # failed to respond (incorrectly)
    # store data for loop (TrialHandler)
    loop.addData('trial_resp.keys',trial_resp.keys)
    loop.addData('trial_resp.corr', trial_resp.corr)
    if trial_resp.keys != None:  # we had a response
        loop.addData('trial_resp.rt', trial_resp.rt)
    loop.addData('trial_resp.started', trial_resp.tStartRefresh)
    loop.addData('trial_resp.stopped', trial_resp.tStopRefresh)
    thisExp.nextEntry()
    
# completed 50 repeats of 'loop'

trialNone = 0; trialBack = 0; hit = 0; miss = 0; fa = 0
for n, t in enumerate(thisExp.entries):
    if t['trial_type'] == 'none':
        trialNone += 1
        if not(t['trial_resp.corr']): fa += 1
    if t['trial_type'] == 'back':
        trialBack += 1
        if t['trial_resp.corr']: hit += 1
        else: miss += 1
logging.log(level=logging.DATA, msg='hit={:.2f}%,miss={:.2f}%,fa={:.2f}%'.format(hit/trialBack*100,miss/trialBack*100,fa/trialNone*100))


# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
